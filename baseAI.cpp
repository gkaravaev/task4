//
// Created by Gregpack on 10-Dec-18.
//

#include "baseAI.h"


void AI::showField(std::ostream & out) {
    for (int j = 0; j <= 9; j++) {
        for (char i = 'A'; i <= 'J'; i++) {
            out << (char)myField.getCell(i, j);
        }
        out << std::endl;
    }
}

void AI::showEnemyField(std::ostream & out) {
    for (int j = 0; j <= 9; j++) {
        for (char i = 'A'; i <= 'J'; i++) {
            out << (char)enemyField.getCell(i, j);
        }
        out << std::endl;
    }
}

std::pair<char, int> AI::finisher(std::vector <std::pair <char, int>> & shotCells) {
    std::vector <std::pair <char, int>> targets;
    std::srand(static_cast<unsigned int>(time(nullptr)));
    if (shotCells.size() == 1) { //либо просто выстрелить, если добивание одной клетки
        char x = shotCells[0].first;
        int y = shotCells[0].second;
        if (enemyField.getCell(x + 1, y) == Cell::EmptyUnchecked) targets.emplace_back(x + 1, y);
        if (enemyField.getCell(x - 1, y) == Cell::EmptyUnchecked) targets.emplace_back(x - 1, y);
        if (enemyField.getCell(x, y + 1) == Cell::EmptyUnchecked) targets.emplace_back(x, y + 1);
        if (enemyField.getCell(x, y - 1) == Cell::EmptyUnchecked) targets.emplace_back(x, y - 1);
    }
    else {
        if (shotCells[0].first == shotCells[shotCells.size() - 1].first) {
            int max = -1; //либо если надо добить 2 или 3 клетки, найти куда стрелять и выстрелить
            for (int i = 0; i < shotCells.size(); i++)
                if (shotCells[i].second > max) max = shotCells[i].second;
            if (enemyField.getCell(shotCells[0].first, max + 1) == Cell::EmptyUnchecked) targets.emplace_back(shotCells[0].first, max + 1);
            if (enemyField.getCell(shotCells[0].first, max - shotCells.size()) == Cell::EmptyUnchecked) targets.emplace_back (shotCells[0].first, max - shotCells.size());
        }
        else {
            char max = 'A' - 1;
            for (int i = 0; i < shotCells.size(); i++)
                if (shotCells[i].first > max) max = shotCells[i].first;
            if (enemyField.getCell(max + 1, shotCells[0].second) == Cell::EmptyUnchecked) targets.emplace_back(max + 1, shotCells[0].second);
            if (enemyField.getCell(max - shotCells.size(), shotCells[0].second) == Cell::EmptyUnchecked) targets.emplace_back (max - shotCells.size(), shotCells[0].second);
        }
    }
    return targets[rand() %  targets.size()];
}

void AI::setShipWaters(std::vector<std::pair<char, int>> & deadShipCells) {
    for (int i = 0; i < deadShipCells.size(); i++) {
        for (char x = deadShipCells[i].first - 1; x <= deadShipCells[i].first + 1; x++) {
            for (int y = deadShipCells[i].second - 1; y <= deadShipCells[i].second + 1; y++) {
                if (enemyField.getCell(x, y) != Cell::DeadShip)
                    enemyField.setCell(x, y, Cell::ShipWater);
            }
        }
    }
}

void AI::showArrangementField(std::ostream & out) {
    for (int j = 0; j <= 9; j++) {
        for (char i = 'A'; i <= 'J'; i++) {
            if (myField.getCell(i, j) == Cell::Ship)
                out << 1;
            else
                out << 0;
        }
        out << std::endl;
    }
}


