#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include "AI_bank.h"
#include <sstream>

TEST_CASE ("Field") {
    gameField testField;
    bool isFieldCorrect = true;
    for (int i = 0; i < 10; i++) {
        for (char j = 'A'; j <= 'J'; j++) {
            if (testField.getCell(j, i) != Cell::EmptyUnchecked) isFieldCorrect = false;
        }
    }
    REQUIRE(isFieldCorrect);
    testField.setCell('J', 0, Cell::Ship);
    REQUIRE(testField.getCell('J', 0) == Cell::Ship);
    REQUIRE(testField.getCell('J', 1) == Cell::EmptyUnchecked);
    REQUIRE(testField.getCell('J', 100) == Cell::Error);
    REQUIRE(testField.isAvailable('J', 1));
    REQUIRE(!testField.isAvailable('J', 0));
    REQUIRE(!testField.isAvailable('J', 100));
}



