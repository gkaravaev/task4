//
// Created by Gregpack on 10-Dec-18.
//

#ifndef BATTLESHIP_GAMEFIELD_H
#define BATTLESHIP_GAMEFIELD_H

#include <map>
#include <utility>
#include <cstdlib>
#include <ctime>
#include <iostream>

//Cell::Error = non-initialized field of map

enum class Cell {
    Error = '0',
    EmptyUnchecked = 'o',
    Ship = '#',
    DeadShip = 'X',
    Checked = '-',
    ShipWater = '~'
};


class gameField {
private:
    std::map <std::pair <char, int>, Cell> field;
    void forcePlaceShip (int, char, int, bool);
public:
    gameField();
    void setCell(char, int, Cell);
    Cell getCell(char, int);
    bool isAvailable (char, int);
    void fitShip (int, char = 'A', char = 'J', int = 0, int = 9);

};


#endif //BATTLESHIP_GAMEFIELD_H
