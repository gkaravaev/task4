//
// Created by Gregpack on 10-Dec-18.
//

#ifndef BATTLESHIP_AI_H
#define BATTLESHIP_AI_H

#include "gameField.h"
#include <vector>

class AI {
protected:
    gameField myField;
    gameField enemyField;
public:
    virtual void arrangement() = 0;
    virtual std::pair<char, int> shoot() = 0;
    virtual void game(std::istream &, std::ostream &) = 0;
    std::pair <char, int> finisher (std::vector <std::pair <char, int>> &);
    void showField(std::ostream &);
    void showEnemyField (std::ostream &);
    void showArrangementField(std::ostream &);
    void setShipWaters (std::vector <std::pair <char, int>> &);
};


#endif //BATTLESHIP_AI_H
