//
// Created by Gregpack on 10-Dec-18.
//

#include "AI_bank.h"

void randomAI::arrangement() {
    for (int i = 4; i >= 1; i--) {
        for (int j = i; j <= 4; j++) {
            myField.fitShip(i);
        }
    }

}

std::pair<char, int> randomAI::shoot() {
    std::srand(static_cast<unsigned int>(time(nullptr)));
    char x = 0;
    int y = 0;
    do {
        x = rand() % 10 + 'A';
        y = rand() % 10;
    } while (!enemyField.isAvailable(x, y));
    return std::make_pair(x, y);
}

void randomAI::game(std::istream & in, std::ostream & out) {
    std::string command;
    while (true) {
        if (command != "Kill")
            in >> command;
        else command = "Shoot!";
        if (command == "Win!" || command == "Lose")
            break;
        else if (command == "Arrange!") {
            arrangement();
            showArrangementField(out);
            continue;
        }
        else if (command == "Shoot!") {
            std::pair <char, int> where = shoot();
            out << where.first << " " << where.second << std::endl;
            in >> command;
            if (command == "Miss") {
                enemyField.setCell(where.first, where.second, Cell::Checked);\
                continue;
            }
            else if (command == "Kill") {
                enemyField.setCell(where.first, where.second, Cell::DeadShip);
                std::vector <std::pair <char, int> > deadShip;
                deadShip.emplace_back(where.first, where.second);
                setShipWaters(deadShip);
                //showEnemyField(out);
                continue;
            }
            else if (command == "Hit") {
                enemyField.setCell(where.first, where.second, Cell::DeadShip);
                std::vector <std::pair <char, int> > deadShip;
                deadShip.emplace_back(where.first, where.second);
                do {
                    std::pair <char, int> whereShoot;
                    whereShoot = finisher(deadShip);
                    out << whereShoot.first << " " << whereShoot.second << std::endl;
                    do {
                        in >> command;
                    } while (command != "Miss" && command != "Kill" && command != "Hit");
                    if (command == "Kill") {
                        enemyField.setCell(whereShoot.first, whereShoot.second, Cell::DeadShip);
                        deadShip.emplace_back(whereShoot.first, whereShoot.second);
                        break;
                    }
                    else if (command == "Miss") {
                        enemyField.setCell(whereShoot.first, whereShoot.second, Cell::Checked);
                        do {
                            in >> command;
                        } while (command != "Shoot!");
                    }
                    else if (command == "Hit") {
                        enemyField.setCell(whereShoot.first, whereShoot.second, Cell::DeadShip);
                        deadShip.emplace_back(whereShoot.first, whereShoot.second);
                    }
                } while (true);
                setShipWaters(deadShip);
                //showEnemyField(out);
                continue;
            }
        }
    }
}

