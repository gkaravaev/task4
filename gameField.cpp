//
// Created by Gregpack on 10-Dec-18.
//

#include "gameField.h"

void gameField::setCell(char x, int y, Cell value) {
    if (getCell(x, y) == Cell::Error)
        return;
    else
        field[std::make_pair(x, y)] = value;
}

Cell gameField::getCell(char x, int y) {
    if (field.find(std::make_pair(x, y)) == field.end())
        return Cell::Error;
    else
        return field[std::make_pair(x, y)];

}

gameField::gameField() {
    for (char i = 'A'; i <= 'J'; i++) {
        for (int j = 0; j <= 9; j++) {
            field[std::make_pair(i, j)] = Cell::EmptyUnchecked;
        }
    }
}

bool gameField::isAvailable(char x, int y) {
    return getCell(x, y) == Cell::EmptyUnchecked;
}

void gameField::fitShip(int size, char lBorder, char rBorder, int uBorder, int dBorder) {
    std::srand(static_cast<unsigned int>(time(nullptr)));
    char x = 0;
    int y = 0;
    while (true) {
        do {
            x = rand() % (rBorder - lBorder + 1 - (size - 1)) + lBorder;
            y = rand() % (dBorder - uBorder + 1 - (size - 1)) + uBorder;
        } while (!isAvailable(x, y));  //choose available cell
        bool downAvailable = true, rightAvailable = true;
        for (int i = 0; i < size; i++) {
            if (!isAvailable(x + i, y)) rightAvailable = false;
            if (!isAvailable(x, y + i)) downAvailable = false;
        } //check if ship can fit somewhere
        if (!rightAvailable && !downAvailable) continue;
        else if (rightAvailable && !downAvailable) {
            forcePlaceShip(size, x, y, true);
            break;
        }
        else if (!rightAvailable && downAvailable) {
            forcePlaceShip(size, x, y, false);
            break;
        }
        else {
            forcePlaceShip(size, x, y, rand() % 2);
            break;
        }

    }
}

void gameField::forcePlaceShip(int size, char x1, int y1, bool isRight) { //true = right, false = down
    if (isRight) {
        setCell(x1 - 1, y1 - 1, Cell::ShipWater);
        setCell(x1 - 1, y1, Cell::ShipWater);
        setCell(x1 - 1, y1 + 1, Cell::ShipWater);
        for (int i = 0; i < size; i++) {
            setCell(x1 + i, y1 - 1, Cell::ShipWater);
            setCell(x1 + i, y1, Cell::Ship);
            setCell(x1 + i, y1 + 1, Cell::ShipWater);
        }
        setCell(x1 + size, y1 - 1, Cell::ShipWater);
        setCell(x1 + size, y1 + 1, Cell::ShipWater);
        setCell(x1 + size, y1, Cell::ShipWater);
    }
    else {
        setCell(x1 - 1, y1 - 1, Cell::ShipWater);
        setCell(x1, y1 - 1, Cell::ShipWater);
        setCell(x1 + 1, y1 - 1, Cell::ShipWater);
        for (int i = 0; i < size; i++) {
            setCell(x1 - 1, y1 + i, Cell::ShipWater);
            setCell(x1, y1 + i, Cell::Ship);
            setCell(x1 + 1, y1 + i, Cell::ShipWater);
        }
        setCell(x1 - 1, y1 + size, Cell::ShipWater);
        setCell(x1, y1 + size, Cell::ShipWater);
        setCell(x1 + 1, y1 + size, Cell::ShipWater);
    }


}



