//
// Created by Gregpack on 10-Dec-18.
//

#ifndef BATTLESHIP_AI_BANK_H
#define BATTLESHIP_AI_BANK_H


#include "baseAI.h"

class randomAI : public AI {

public:
    randomAI () = default;
    ~randomAI() = default;
    void arrangement() override;
    std::pair<char, int> shoot() override;
    void game(std::istream &, std::ostream &) override;
};


#endif //BATTLESHIP_AI_BANK_H
